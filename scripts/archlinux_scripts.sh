# install packages with pacman 
function install_package() {
	if [ -z $1 ]; then
		echo "package name not specified"
	fi

	echo "Installing $1"
	sudo pacman --noconfirm -S $1 > /dev/null || (echo "$1 installation failed !" && exit)
	echo "done."
}
