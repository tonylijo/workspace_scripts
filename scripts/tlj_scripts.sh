#create a c project with a directory inside workspace
#argument is the directory name
#appropriate ycm extra file will be copied to this directory
function tlj_create_c_project () {
	if [ ! -d "$TLJ_ENV_VAR_WORKSPACE/$1" ]
	then
		mkdir -p $TLJ_ENV_VAR_WORKSPACE/$1;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/ycm/ycm_extra_conf_c.py $TLJ_ENV_VAR_WORKSPACE/$1/.ycm_extra_conf.py;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/Rules.make $TLJ_ENV_VAR_WORKSPACE/$1/Rules.make;
		sync;
		PROGRAM_NAME=$(basename $1)
		echo "PROGRAM=$PROGRAM_NAME" >> $TLJ_ENV_VAR_WORKSPACE/$1/Rules.make;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/Makefile  $TLJ_ENV_VAR_WORKSPACE/$1/Makefile;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/uncrustify.cfg $TLJ_ENV_VAR_WORKSPACE/$1/uncrustify.cfg;
		cd $TLJ_ENV_VAR_WORKSPACE/$1;
	else
		echo "directory allready exist in workspace please select other location"
	fi
}


function tlj_create_bitbucket_project () {
	curl -u tonylijo@gmail.com https://api.bitbucket.org/1.0/repositories/ --data name=$1
}

MPLAYER_MUSIC_STATE=PLAYING
function tlj_mplayer_pause_music () {
	echo "pause" > ~/.mplayer/fifo
	MPLAYER_MUSIC_STATE=PAUSED
}

# create a note in notes directory
# create the note in tdoc framework
function tlj_create_note_common () {
	if [ -z $TLJ_ENV_VAR_NOTES_DIR ]
	then
		echo "TLJ_ENV_VAR_NOTES_DIR is not set please set it first"
		echo "tlj_set_note_dir <path>"
		return 1;
	fi
	if [ ! -d "${TLJ_ENV_VAR_NOTES_DIR}/build" ]
	then
		echo "not build system not preset so cloning"
		git clone https://tonylijo@bitbucket.org/tonylijo/tdoc_build.git ${TLJ_ENV_VAR_NOTES_DIR}/build
		cp ${TLJ_ENV_VAR_NOTES_DIR}/build/core/root.mk ${TLJ_ENV_VAR_NOTES_DIR}/Makefile
	fi

	if [ -z "$1" ]
	then
		echo 'usage: tlj_create_note_common <workspace directory>';
		return 1;
	else

		cd ${TLJ_ENV_VAR_NOTES_DIR}	
		source build/envsetup.sh
		tdoc_create_article $1

	fi
	return 0;
}

# create a note in notes directory
# create the note in tdoc framework
function tlj_create_slideshow_common () {
	if [ -z $TLJ_ENV_VAR_NOTES_DIR ]
	then
		echo "TLJ_ENV_VAR_NOTES_DIR is not set please set it first"
		echo "tlj_set_note_dir <path>"
		return 1;
	fi
	if [ ! -d "${TLJ_ENV_VAR_NOTES_DIR}/build" ]
	then
		echo "not build system not preset so cloning"
		git clone https://tonylijo@bitbucket.org/tonylijo/tdoc_build.git ${TLJ_ENV_VAR_NOTES_DIR}/build
		cp ${TLJ_ENV_VAR_NOTES_DIR}/build/core/root.mk ${TLJ_ENV_VAR_NOTES_DIR}/Makefile
	fi

	if [ -z "$1" ]
	then
		echo 'usage: tlj_create_note_common <workspace directory>';
		return 1;
	else

		cd ${TLJ_ENV_VAR_NOTES_DIR}	
		source build/envsetup.sh
		tdoc_create_slideshow $1

	fi
	return 0;
}



#create a android service project with a directory inside workspace
#argument is the directory name
#appropriate ycm extra file will be copied to this directory
function tlj_create_android_service_project () {
	if [ -z $TLJ_ENV_VAR_AOSP_ROOT ]
	then
		echo "TLJ_ENV_VAR_AOSP_ROOT is not not set please set it first"
		echo "tlj_set_aosp_root <path>"
		return 1;
	fi

	if [ ! -d "$TLJ_ENV_VAR_AOSP_ROOT/$1" ]
	then
		mkdir -p $TLJ_ENV_VAR_AOSP_ROOT/$1;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/ycm/ycm_extra_conf_android_service.py $TLJ_ENV_VAR_AOSP_ROOT/$1/.ycm_extra_conf.py;
		sync;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/android_service_makefiles/Android.mk  $TLJ_ENV_VAR_AOSP_ROOT/$1/Android.mk;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/uncrustify.cfg $TLJ_ENV_VAR_AOSP_ROOT/$1/uncrustify.cfg;
		cd $TLJ_ENV_VAR_AOSP_ROOT/$1;
	else
		echo "directory allready exist in workspace please select other location"
	fi
	return 0;
}

#create a android service project with a directory inside workspace
#argument is the directory name
#appropriate ycm extra file will be copied to this directory
function tlj_create_android_hal_project () {
	if [ -z $TLJ_ENV_VAR_AOSP_ROOT ]
	then
		echo "TLJ_ENV_VAR_AOSP_ROOT is not not set please set it first"
		echo "tlj_set_aosp_root <path>"
		return 1;
	fi

	if [ ! -d "$TLJ_ENV_VAR_AOSP_ROOT/hardware/$1" ]
	then
		mkdir -p $TLJ_ENV_VAR_AOSP_ROOT/hardware/$1;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/ycm/ycm_extra_conf_android_hal.py $TLJ_ENV_VAR_AOSP_ROOT/hardware/$1/.ycm_extra_conf.py;
		sync;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/android_hal_makefiles/Android.mk  $TLJ_ENV_VAR_AOSP_ROOT/hardware/$1/Android.mk;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/uncrustify.cfg $TLJ_ENV_VAR_AOSP_ROOT/hardware/$1/uncrustify.cfg;
		cd $TLJ_ENV_VAR_AOSP_ROOT/hardware/$1;
	else
		echo "directory allready exist in workspace please select other location"
	fi
	return 0;
}

#create a cuda project with a directory inside workspace
#argument is the directory name
#appropriate ycm extra file will be copied to this directory
function tlj_create_cuda_project () {
	if [ ! -d "$TLJ_ENV_VAR_WORKSPACE/$1" ]
	then
		mkdir -p $TLJ_ENV_VAR_WORKSPACE/$1;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/ycm/ycm_extra_conf_cpp.py $TLJ_ENV_VAR_WORKSPACE/$1/.ycm_extra_conf.py;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/cuda_makefiles/Rules.make $TLJ_ENV_VAR_WORKSPACE/$1/Rules.make;
		sync;
		PROGRAM_NAME=$(basename $1)
		echo "PROGRAM=$PROGRAM_NAME" >> $TLJ_ENV_VAR_WORKSPACE/$1/Rules.make;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/cuda_makefiles/Makefile  $TLJ_ENV_VAR_WORKSPACE/$1/Makefile;
		cd $TLJ_ENV_VAR_WORKSPACE/$1;
	else
		echo "directory allready exist in workspace please select other location"
	fi
}


#create a cpp project with a directory inside workspace
#argument is the directory name
#appropriate ycm extra file will be copied to this directory
function tlj_create_cpp_project () {
	if [ ! -d "$TLJ_ENV_VAR_WORKSPACE/$1" ]
	then
		mkdir -p $TLJ_ENV_VAR_WORKSPACE/$1;
		cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/ycm/ycm_extra_conf_cpp.py $TLJ_ENV_VAR_WORKSPACE/$1/.ycm_extra_conf.py;
		cd $TLJ_ENV_VAR_WORKSPACE/$1;
	else
		echo "directory allready exist in workspace please select other location"
	fi
}

#print the current workspace directory
#TLJ_ENV_VAR_WORKSPACE points to the current workspace
function tlj_workspace () {
	cd $TLJ_ENV_VAR_WORKSPACE; 
	echo "changed to directory $TLJ_ENV_VAR_WORKSPACE";
}

# change the driectory to imx7 product directory
function tlj_products_imx7 () {
	cd $TLJ_ENV_VAR_WORKSPACE/products/imx7
}

#print the current workspace directory
#TLJ_ENV_VAR_AOSP_ROOT points to the current workspace
function tlj_aosp_root () {
	cd $TLJ_ENV_VAR_AOSP_ROOT; 
	echo "changed to directory $TLJ_ENV_VAR_AOSP_ROOT";
}

#print the current workspace directory
#TLJ_ENV_VAR_NOTES_DIR points to the current workspace
function tlj_notespace () {
	cd $TLJ_ENV_VAR_NOTES_DIR;
	echo "changed to directory $TLJ_ENV_VAR_NOTES_DIR";
}


#print the current workspace directory
#TLJ_ENV_VAR_WORKSPACE points to the current workspace
function tlj_projects() {
	cd $TLJ_ENV_VAR_PROJECTS;
	echo "changed to directory $TLJ_ENV_VAR_WORKSPACE";
}

#setup a new workspace directory
function tlj_set_workspace() {

	if [ -z "$1" ]
	then
		echo 'usage: tlj_set_workspace <workspace directory>';
		return;
	fi

	echo "changing workspace from $TLJ_ENV_VAR_WORKSPACE to $1";

	if [ ! -d "$1" ] 
	then
		read -n 1 -p "workspace $1 is not present do you want to create a new one (y/n)?" TLJ_CHOICE_ANS;
		case $TLJ_CHOICE_ANS in
			y)
				mkdir -p $1;;
			*)
				exit;;
		esac
	fi
	TLJ_ENV_VAR_WORKSPACE=$1
}

#setup a new testspace directory
function tlj_set_testspace () {
	if [ -z "$1" ]
	then
		echo 'usage: tlj_set_testspace <testpace directory>'
		return;
	fi

	echo "changing testspace from $TLJ_ENV_VAR_TESTSPACE to $1";
	if [ ! -d "$1" ] 
	then
		read -n 1 -p "testspace $1 is not present do you want to create a new one (y/n)? " TLJ_CHOICE_ANS;
		case $TLJ_CHOICE_ANS in
			y)
				mkdir -p $1;;
			*)
				exit;;
		esac
	fi
	TLJ_ENV_VAR_TESTSPACE=$1
}

#This function will make zimage from the current directory
#env.sh file should be present in the current directory
function tlj_make_kernel_zimage() {
	if [ ! -f "./env.sh" ] 
	then
		echo "You don't have env.sh in your current directory"
	else
		`cat env.sh`
		make ARCH=arm zImage -j4
	fi
}

#This function will make kernel modules from the current directory
#tlj_make_kernel_modules will check for env.sh in the current directory
#for exporting the required enviornment variables
function tlj_make_kernel_modules() {	
	if [ ! -f "./env.sh" ] 
	then
		echo "You don't have env.sh in your current directory"
	else 
		`cat env.sh`
		make ARCH=arm modules -j4
		make ARCH=arm modules_install INSTALL_MOD_PATH=./modules_`date +"%d_%b_%y_%H_%M_%S"`
	fi
}

function tlj_ssh_to_build_pc() {
	ssh -Y $TLJ_ENV_BUILD_PC_USER@$TLJ_ENV_BUILD_PC_IP
}

function tlj_ssh_to_jarvis() {
	ssh -Y $TLJ_JARVIS_USER@$TLJ_JARVIS_IP
}

function tlj_create_generic_c_makefile() {
	cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/ycm/ycm_extra_conf_c.py .;
	cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/Rules.make .;
	cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/Makefile  .;
	cp ${TLJ_ENV_VAR_TOOLS_DIR}/conf/samples/c_makefiles/uncrustify.cfg .;
}

function tlj_create_generic_cpp_makefile() {
	:
}

function tlj_sniff_http() {
	PORT=80;
	if [ ! -z $1 ] 
	then
		PORT=$i;
	fi
	sudo tcpdump -A -s 0 'tcp dst port 8000 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)'
}

#setup REMOTE ATTRIBUTES
function tlj_set_remote() {
	read -r -p "what is your target ip:" TLJ_ENV_VAR_REMOTE_IP;
	read -r -p "what is your remote username:" TLJ_ENV_VAR_REMOTE_USER;
}

#print remote / target attributes
function tlj_print_remote() {
	echo "remote ip : $TLJ_ENV_VAR_REMOTE_IP"
	echo "remote user: $TLJ_ENV_VAR_REMOTE_USER"
}

#sync file from remote to your pc
function tlj_sync_to_remote() {
	if [ -z $TLJ_ENV_VAR_REMOTE_IP ] && [ -z $TLJ_ENV_VAR_REMOTE_USER] ; then
		echo "TLJ_ENV_TARGET_IP,TLJ_ENV_VAR_TARGET_USER not set please set it in env.sh"
	fi

	
	if [ -z $1 ]; then
		rsync --delete -r . $TLJ_ENV_VAR_REMOTE_USER@$TLJ_ENV_VAR_REMOTE_IP:$(basename $PWD)
	else 
		rsync --delete -r $1 $TLJ_ENV_VAR_REMOTE_USER@$TLJ_ENV_VAR_REMOTE_IP:$1
	fi
}

function tlj_youtube_mp3_extract() {
	if [ -z $1 ] ; then
		echo "url not specified"
	fi
	
	youtube-dl --no-check-certificate --extract-audio --audio-format mp3  $1
}

# get the system volume
# value in range 0 - 255
function tlj_get_volume() {
	amixer get PCM
}

# change system volume
# the value in range 0 - 255
function tlj_set_volume() {
	if [ -z $1 ]; then
		echo "usage tlj_set_volume <volume>"
		return 1
	fi
	amixer set PCM $1
}
