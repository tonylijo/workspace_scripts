#!/bin/bash
source env.sh

#set backup enviornment variable
function tlj_set_backup_var() {
	if [ -z "$1" ]; then
		echo "backup path not specified\n";
	fi
	if [ ! -z "$TLJ_ENV_VAR_BACKUP_PATH" ]
	then
		echo "old backup path : $TLJ_ENV_VAR_BACKUP_PATH";
	fi
	TLJ_ENV_VAR_BACKUP_PATH=$1;
}

#create a new backup directory and setup back enviornment vaiable to
#that directory
function tlj_setup_new_backup_path() {
	if [ -z $TLJ_ENV_VAR_TOOLS_DIR ]; then
		echo "TLJ_ENV_VAR_TOOLS_DIR Not set please check your env.sh"
		return;
	fi
	TLJ_ENV_VAR_BACKUP_PATH=$TLJ_ENV_VAR_TOOLS_DIR/backup/tools_`date +"%d_%m_%y_%H_%M_%S"`;
	echo "creating backup directory $TLJ_ENV_VAR_BACKUP_PATH";

	mkdir -p $TLJ_ENV_VAR_BACKUP_PATH ;
}

#backup old dotfiles
function tlj_backup_old_dotfiles() {
	tlj_setup_new_backup_path;
	for value in ${TLJ_ENV_VAR_TOOLS_DIR}/conf/dotfiles/* ; do
		echo "copying $HOME/.$(basename ${value}) to ${TLJ_ENV_VAR_BACKUP_PATH}/";
		mv $HOME/.$(basename ${value}) $TLJ_ENV_VAR_BACKUP_PATH/$(basename ${value})_`date +"%d_%m_%y_%H_%M_%S"`;
		echo "done."
	done
}

#install new dotfiles to home directory
function tlj_install_dotfiles() {
	for value in ${TLJ_ENV_VAR_TOOLS_DIR}/conf/dotfiles/* ; do
		echo "ln -s ${value} $HOME/.$(basename ${value})"
		ln -s ${value} $HOME/.$(basename ${value})
	done
}

#Find linux distro in use
function tlj_find_linux_distro() {
	if [ -f /etc/redhat-release ]; then
		TLJ_ENV_VAR_LINUX_DIST='RedHat'
		source $TLJ_ENV_VAR_TOOLS_DIR/scripts/redhat_scripts.sh
	elif [ -f /etc/SuSe-release ]; then
		TLJ_ENV_VAR_LINUX_DIST='SuSE';
		source $TLJ_ENV_VAR_TOOLS_DIR/scripts/suse_scripts.sh
	elif [ -f /etc/mandrake-release ]; then
		TLJ_ENV_VAR_LINUX_DIST='Mandrake';
		source $TLJ_ENV_VAR_TOOLS_DIR/scripts/mandrake_scripts.sh
	elif [ -f /etc/debian_version ]; then
		TLJ_ENV_VAR_LINUX_DIST='Debian';
		ls -l $TLJ_ENV_VAR_TOOLS_DIR/scripts/debian_scripts.sh
		source $TLJ_ENV_VAR_TOOLS_DIR/scripts/debian_scripts.sh
	elif [ -f /etc/pacman.conf ]; then
		TLJ_ENV_VAR_LINUX_DIST='Archlinux'
		ls -l $TLJ_ENV_VAR_TOOLS_DIR/scripts/archlinux_scripsts.sh
		source $TLJ_ENV_VAR_TOOLS_DIR/scripts/archlinux_scripts.sh
	fi	

	echo "found linux distro: $TLJ_ENV_VAR_LINUX_DIST"
}

#Find os in use
function tlj_find_os() {
	TLJ_ENV_VAR_OS=`uname -s`;
	if [ "${TLJ_ENV_VAR_OS}" = "SunOS" ]; then
		TLJ_ENV_VAR_OS=Solaris
	elif [ "${TLJ_ENV_VAR_OS}" = "Darwin" ]; then
		TLJ_ENV_VAR_OS=MacOS
	elif [ "${TLJ_ENV_VAR_OS}" = "Linux" ]; then
		tlj_find_linux_distro;
	fi
	echo "found os type: $TLJ_ENV_VAR_OS"
}

function setup_powerlinefonts()
{
	pushd /tmp
	git clone https://github.com/powerline/fonts.git
	pushd fonts
	if ./install.sh
	then
		echo "powerline fonts installed"
	fi
}

function install_base_devel_packages()
{
	if [ "$TLJ_ENV_VAR_LINUX_DIST" == "Archlinux" ]; then
		install_package base-devel
	elif [ "$TLJ_ENV_VAR_LINUX_DIST" == "Debian" ]; then
		install_package build-essential
	else
		echo "please run tlj_find_os first"
	fi
}

function setup() {
	install_base_devel_packages;
	install_package cmake; 
	install_package tmux; 
	install_package vim; 
	install_package ctags; 
	install_package uncrustify;
	install_package minicom;
	install_package v4l-utils;
	#git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim;
	#vim -E -u NONE -S ~/.vimrc +PluginInstall +qall;
	#cd ~/.vim/bundle/YouCompleteMe && ./install.py --clang-completer
}

tlj_find_os;
setup;
tlj_backup_old_dotfiles;
tlj_install_dotfiles;
