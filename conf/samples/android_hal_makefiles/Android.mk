LOCAL_PATH := $(call my-dir)

# HAL module implemenation, not prelinked and stored in hw/
include $(CLEAR_VARS)
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw
LOCAL_SHARED_LIBRARIES := liblog libcutils
LOCAL_SRC_FILES :=
LOCAL_MODULE := lights.$(TARGET_BOARD_PLATFORM)
LOCAL_MODULE_TAGS := eng
include $(BUILD_SHARED_LIBRARY)
