LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:=  <src_file1> \
	<src_file2> \
	<src_file3 \
	

LOCAL_STATIC_LIBRARIES := \
    libutils \
    libbinder

LOCAL_MODULE:= <name of module> 

LOCAL_MODULE_TAGS := optional

LOCAL_CFLAGS += <cflags if any> 

include $(BUILD_STATIC_LIBRARY)
